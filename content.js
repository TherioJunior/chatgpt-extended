// Create the ChatGPT Addon button
const button = document.createElement('button');

button.innerText = 'ChatGPT Extended';
button.id = 'chatgpt-addon-button';
document.body.appendChild(button);

// When button is clicked, toggle the embedded popup
button.addEventListener('click', () => {
	const popup = document.getElementById('chatgpt-addon-popup');

	if (popup.style.display === 'none') {
		popup.style.display = 'block';
	} else {
		popup.style.display = 'none';
	}
});

// Create the embedded popup
const popup = document.createElement('div');

popup.id = 'chatgpt-addon-popup';
popup.style.display = 'none';
document.body.appendChild(popup);

// Fetch popup content and embed it
fetch(browser.runtime.getURL('popup/popup.html'))
	.then(response => response.text())
	.then(data => {
		var parser = new DOMParser();
		var doc = parser.parseFromString(data, 'text/html');
		var popupContent = doc.body.childNodes;
		while (popupContent.length > 0) {
			popup.appendChild(popupContent[0]);
		}
	});


const getDefaultGPTVersion = async () => {
	return new Promise((resolve) => {
		browser.storage.local.get('gpt_version', (data) => {
			// Read the set model from browser storage, or select the free GPT-3.5 build to prevent locking non-premium users out.
			const defaultVersion = data['gpt_version'] || 'text-davinci-002-render-sha';

			resolve(defaultVersion);
		});
	});
}

const updateDefaultGPTVersion = async () => {
	// TODO: Do this without forcing a full reload of the page.
	const targetDomain = 'chat.openai.com';
	const currentURL = new URL(window.location.href);
	const forceSpecificModelCheckbox = document.getElementById("force-specific-model");

	if (!forceSpecificModelCheckbox || !forceSpecificModelCheckbox.checked)
		return;

	if (currentURL.pathname.startsWith('/c/') || currentURL.pathname.startsWith('/auth/'))
		return;

	// Check if current URL's domain matches the target domain
	if (currentURL.hostname === targetDomain) {
		const defaultVersion = await getDefaultGPTVersion();

		if (currentURL.searchParams.get('model') ===  defaultVersion) {
			return;
		}

		// Set or update the 'model' query param to the saved default model version
		currentURL.searchParams.set('model', defaultVersion);

		// Redirect to the chat page using the saved default model version
		setTimeout(() => {
			window.location.href = currentURL.toString();
		}, 500);
	}
}

function periodicallyRepeatChecks() {
	setInterval(() => {
		updateDefaultGPTVersion();

		const textarea = document.getElementById("prompt-textarea");
		const overlayContainerExists = document.getElementById("overlay-container");

		const defaultLanguageLabel = document.getElementById("default-language-select-label");
		const defaultLanguageDropdown = document.getElementById("default-language-select");
		const writingToneLabel = document.getElementById("writing-tone-select-label");
		const writingToneDropdown = document.getElementById("writing-tone-select");
		const writingStyleLabel = document.getElementById("writing-style-select-label");
		const writingStyleDropdown = document.getElementById("writing-style-select");
		const continueConversationButton = document.getElementById("continue-conversation-button");

		if (textarea) {
			const overlayContainer = document.createElement("div");
			overlayContainer.id = "overlay-container";
			overlayContainer.className = "overlay-container";

			// TODO: Force chatbox height to be smaller to compensate for the extra extension content

			if (!defaultLanguageDropdown) {
				const defaultLanguageDropdown = document.createElement("select");
				defaultLanguageDropdown.id = "default-language-select";
				defaultLanguageDropdown.style.display = "none";

				let label = document.createElement('label');
				label.setAttribute('for', 'default-language-select');
				label.id = "default-language-select-label";
				label.textContent = 'Language:';
				label.style.display = "none"

				const options = [
					{ value: "default", text: "Default" },
					{ value: "auto-detect", text: "Auto-detect" },
					{ value: "en", text: "English" },
					{ value: "de", text: "German" }
				];

				options.forEach(optionData => {
					const option = document.createElement("option");
					option.value = optionData.value;
					option.textContent = optionData.text;
					defaultLanguageDropdown.appendChild(option);
				});

				overlayContainer.appendChild(label);
				overlayContainer.appendChild(defaultLanguageDropdown);
			}

			if (!writingToneDropdown) {
				const writingToneDropdown = document.createElement("select");
				writingToneDropdown.id = "writing-tone-select";
				writingToneDropdown.style.display = "none";

				let label = document.createElement('label');
				label.setAttribute('for', 'writing-tone-select');
				label.id = "writing-tone-select-label";
				label.textContent = 'Tone:';
				label.style.display = "none";

				const options = [
					{ value: "default", text: "Default" },
					{ value: "authoritative", text: "Authoritative" },
					{ value: "cold", text: "Cold" },
					{ value: "direct", text: "Direct" },
					{ value: "playful", text: "Playful" }
				];

				options.forEach(optionData => {
					const option = document.createElement("option");
					option.value = optionData.value;
					option.textContent = optionData.text;
					writingToneDropdown.appendChild(option);
				});

				overlayContainer.appendChild(label);
				overlayContainer.appendChild(writingToneDropdown);
			}

			if (!writingStyleDropdown) {
				const writingStyleDropdown = document.createElement("select");
				writingStyleDropdown.id = "writing-style-select";
				writingStyleDropdown.style.display = "none";

				let label = document.createElement("label");
				label.setAttribute("for", "writing-style-select");
				label.id = "writing-style-select-label";
				label.textContent = "Style:";
				label.style.display = "none";

				const options = [
					{ value: "default", text: "Default" },
					{ value: "analytical", text: "Analytical" },
					{ value: "conversational", text: "Conversational" },
					{ value: "creative", text: "Creative" },
					{ value: "critical", text: "Critical" },
					{ value: "eli5", text: "ELI5" },
					{ value: "informative", text: "Informative" },
					{ value: "journalistic", text: "Journalistic" },
					{ value: "precise", text: "Precise" },
					{ value: "summarise", text: "Summarise" },
					{ value: "technical", text: "Technical" }
				];

				options.forEach(optionData => {
					const option = document.createElement("option");
					option.value = optionData.value;
					option.textContent = optionData.text;
					writingStyleDropdown.appendChild(option);
				});

				overlayContainer.appendChild(label);
				overlayContainer.appendChild(writingStyleDropdown);
			}

			if (!continueConversationButton) {
				const continueConversationButton = document.createElement("button");
				continueConversationButton.id = "continue-conversation-button";
				continueConversationButton.innerHTML = "&#9654; Continue conversation";
				continueConversationButton.style.display = "none";

				overlayContainer.appendChild(continueConversationButton);
			}

			setTimeout(() => {
				if (modelHasNoAttachmentOptions()) {
					defaultLanguageDropdown.setAttribute('style', 'right: -17px');
					defaultLanguageLabel.setAttribute('style', 'right: -17px');
					writingToneDropdown.setAttribute('style', 'right: -172px');
					writingToneLabel.setAttribute('style', 'right: -172px');
					writingStyleDropdown.setAttribute("style", "right: -327px");
					writingStyleLabel.setAttribute("style", "right: -327px");
				} else {
					defaultLanguageDropdown.setAttribute("style", "right: -45px");
					defaultLanguageLabel.setAttribute('style', 'right: -45px');
					writingToneDropdown.setAttribute('style', 'right: -200px');
					writingToneLabel.setAttribute('style', 'right: -200px');
					writingStyleDropdown.setAttribute("style", "right: -355px");
					writingStyleLabel.setAttribute("style", "right: -355px");
				}

				defaultLanguageDropdown.style.display = "";
				defaultLanguageLabel.style.display = "";
				writingToneDropdown.style.display = "";
				writingToneLabel.style.display = "";
				writingStyleDropdown.style.display = "";
				writingStyleLabel.style.display = "";

				continueConversationButton.style.display = isInsideChat() ? "block" : "none";
			}, 1500);

			if (!overlayContainerExists)
				textarea.parentNode.appendChild(overlayContainer);
		}
	}, 1000);
}

updateDefaultGPTVersion();
periodicallyRepeatChecks();

// Helper function
function modelHasNoAttachmentOptions() {
	if (window.location.href.includes("gpt-4-code-interpreter") || window.location.href === "https://chat.openai.com/?model=gpt-4")
		return false;

	return document.querySelector('[aria-label="Attach files"]') === null;
}
