let prompt = ""; // Used to store the actual prompt instructions which are appended before the users' own question/prompt/whatever
let backupPrompt = ""; // Used to clear prompt adjustments when all adjustment items are disabled/on default without clearing what's already written inside the chatbox
let tempDefaultLanguageString = ""; // Used to store what each language adjustment adds to the prompt
let tempWritingToneString = ""; // Used to store what each writing tone adjustment adds to the prompt
let tempWritingStyleString = "";
let forceFollowInstructions = "These instructions are to be strictly followed without exceptions, but not to be commented on. You MUST uphold this for the entirety of the conversation.\n\n";
let clearedChatboxAfterEnteringChat = true; // Used to clear the chatbox one single time after entering a chat, to clear the prompt adjustments
let newValue = "";

setTimeout(() => {
    /* Set initial version options on the first extension load */
    updateGPTVersionOptions("free");
}, 1500);

document.body.addEventListener("mouseover", (e) => {
    const textarea = document.getElementById("prompt-textarea");

    const forceSpecificModelCheckbox = document.getElementById("force-specific-model");
    const chatgptSubscriptionType = document.getElementById("subscription_type");
    const chatgptDefaultVersion = document.getElementById("gpt_version");

    const defaultLanguageDropdown = document.getElementById("default-language-select");
    const writingToneDropdown = document.getElementById("writing-tone-select");
    const writingStyleDropdown = document.getElementById("writing-style-select");

    if (!clearedChatboxAfterEnteringChat && isInsideChat()) {
        if (textarea.value.includes("Instructions:"))
            setChatboxRaw("", false);
        clearedChatboxAfterEnteringChat = true;
    } else {
        clearedChatboxAfterEnteringChat = false;
    }

    browser.storage.local.get(["force_specific_model", "subscription_type", "gpt_version", "default_language_select", "writing_tone_select", "writing_style_select"], (data) => {
        if (data.force_specific_model && e.target.id === "chatgpt-addon-button") {
            forceSpecificModelCheckbox.checked = data.force_specific_model;

            // Manually dispatch the change event
            let event = new Event("change", {
                "bubbles": true,
                "cancelable": true
            });

            forceSpecificModelCheckbox.dispatchEvent(event);
        }

        if (data.subscription_type && e.target.id === "chatgpt-addon-button") {
            chatgptSubscriptionType.value = data.subscription_type;
            updateGPTVersionOptions(data.subscription_type);
        }

        if (data.gpt_version && e.target.id === "chatgpt-addon-button")
            chatgptDefaultVersion.value = data.gpt_version;

        // TODO: Test how preparing this event outside the if statement affects performance, to avoid creating a separate event variable for each of the two dropdowns
        if (data.writing_style_select && writingStyleDropdown.value !== data.writing_style_select) {
            writingStyleDropdown.value = data.writing_style_select;

            // Manually dispatch the change event
            let event = new Event("change", {
                "bubbles": true,
                "cancelable": true
            });

            writingStyleDropdown.dispatchEvent(event);
        }

        if (data.default_language_select && defaultLanguageDropdown.value !== data.default_language_select) {
            defaultLanguageDropdown.value = data.default_language_select;

            // Manually dispatch the change event
            let event = new Event("change", {
                "bubbles": true,
                "cancelable": true
            });
            defaultLanguageDropdown.dispatchEvent(event);
        }

        if (data.writing_tone_select && writingToneDropdown.value !== data.writing_tone_select) {
            writingToneDropdown.value = data.writing_tone_select;

            // Manually dispatch the change event
            let event = new Event("change", {
                "bubbles": true,
                "cancelable": true
            });

            writingToneDropdown.dispatchEvent(event);
        }
    });
});

document.body.addEventListener("change", (e) => {
    const textarea = document.getElementById("prompt-textarea");

    const forceSpecificModelCheckbox = document.getElementById("force-specific-model");
    const chatGPTDefaultModelDiv = document.getElementById("chatgpt-default-model");

    const defaultLanguageDropdown = document.getElementById("default-language-select");
    const writingToneDropdown = document.getElementById("writing-tone-select");
    const writingStyleDropdown = document.getElementById("writing-style-select");

    setTimeout(() => {
        const continueConversationButton = document.getElementById("continue-conversation-button");
        continueConversationButton.style.display = isInsideChat() ? "block" : "none";
    }, 1500);

    if (e.target === forceSpecificModelCheckbox) {
        chatGPTDefaultModelDiv.style.display = forceSpecificModelCheckbox.checked ? "flex" : "none";
        browser.storage.local.set({
            "force_specific_model": forceSpecificModelCheckbox.checked
        }, () => {
            // Do nothing
        });
    }

    switch (e.target.id) {
        case "subscription_type":
            updateGPTVersionOptions(e.target.value);
            break;
        case "default-language-select":
            if (defaultLanguageDropdown.value !== "default") {
                switch (defaultLanguageDropdown.value) {
                    case "auto-detect":
                        tempDefaultLanguageString = "The language you are supposed to have this conversation in is the language you detect from what the user is typing. The chat title MUST be in this same language. ";
                        break;
                    case "en":
                        tempDefaultLanguageString = "The language you are supposed to have this conversation in is English. The chat title MUST be in this same language. ";
                        break;
                    case "de":
                        tempDefaultLanguageString = "The language you are supposed to have this conversation in is German. The chat title MUST be in this same language. ";
                        break;
                }

                if (writingToneDropdown.value !== "default" || writingStyleDropdown.value !== "default") {
                    backupPrompt = prompt;
                    prompt = "Instructions: ";
                    prompt += tempDefaultLanguageString;
                    prompt += writingToneDropdown.value !== "default" ? tempWritingToneString : "";
                    prompt += writingStyleDropdown.value !== "default" ? tempWritingStyleString : "";
                }
                else
                    prompt = "Instructions: " + tempDefaultLanguageString + "\n";
            } else {
                prompt = prompt.replace(tempDefaultLanguageString, "");
                setChatboxRaw(textarea.value.replace(tempDefaultLanguageString, ""), false);
            }
            break;
        case "writing-tone-select":
            if (writingToneDropdown.value !== "default") {
                switch (writingToneDropdown.value) {
                    case "authoritative":
                        tempWritingToneString = "Be authoritative and strict with your answers. ";
                        break;
                    case "cold":
                        tempWritingToneString = "Be cold with your answers. ";
                        break;
                    case "direct":
                        tempWritingToneString = "Be direct with your answers, don't sugarcoat anything. ";
                        break;
                    case "playful":
                        tempWritingToneString = "Be playful with your answers. ";
                        break;
                }

                if (defaultLanguageDropdown.value !== "default" || writingStyleDropdown.value !== "default") {
                    backupPrompt = prompt;
                    prompt = "Instructions: ";
                    prompt += defaultLanguageDropdown.value !== "default" ? tempDefaultLanguageString : "";
                    prompt += tempWritingToneString;
                    prompt += writingStyleDropdown.value !== "default" ? tempWritingStyleString : "";
                }
                else
                    prompt = "Instructions: " + tempWritingToneString + "\n";
            } else {
                prompt = prompt.replace(tempWritingToneString, "");
                setChatboxRaw(textarea.value.replace(tempWritingToneString, ""), false);
            }
            break;
        case "writing-style-select":
            if (writingStyleDropdown.value !== "default") {
                switch (writingStyleDropdown.value) {
                    case "analytical":
                        tempWritingStyleString = "Structure your answer to be analytical of the content you are given. ";
                        break;
                    case "conversational":
                        tempWritingStyleString = "Structure your answer to be conversational. Ask questions. Be friendly. ";
                        break;
                    case "creative":
                        tempWritingStyleString = "Structure your answer to be very creative. Try reading into the users' prompts and use all your imagination to answer. "
                        break;
                    case "critical":
                        tempWritingStyleString = "Structure your answer to be critical. You must act as a critic and give constructive criticism about the users' prompts or requests. ";
                        break;
                    case "eli5":
                        tempWritingStyleString = "Structure your answer to be simple. Explain the topic the user is asking about like he is a 5 year old and knows nothing about it. ";
                        break;
                    case "informative":
                        tempWritingStyleString = "Structure your answer to be informative. Say a lot about the users' request and inform him deeply about the topic. ";
                        break;
                    case "journalistic":
                        tempWritingStyleString = "Structure your answer to be journalistic. ";
                        break;
                    case "precise":
                        tempWritingStyleString = "Structure your answer to be precise. Don't go out on tangents or reference not closely related things. Strictly stick to the users' request. ";
                        break;
                    case "summarise":
                        tempWritingStyleString = "Structure your answer to be a summarization. Don't say anything except 'Summarization:' followed by the summarised text. ";
                        break;
                    case "technical":
                        tempWritingStyleString = "Structure your answer to be technical. Imagine you are speaking to a long experienced IT specialist. ";
                        break;
                }

                if (defaultLanguageDropdown.value !== "default" || writingToneDropdown.value !== "default") {
                    backupPrompt = prompt;
                    prompt = "Instructions: ";
                    prompt += defaultLanguageDropdown.value !== "default" ? tempDefaultLanguageString : "";
                    prompt += writingToneDropdown.value !== "default" ? tempWritingToneString : "";
                    prompt += tempWritingStyleString;
                }
                else
                    prompt = "Instructions: " + tempWritingStyleString + "\n";
            } else {
                prompt = prompt.replace(tempWritingStyleString, "");
                setChatboxRaw(textarea.value.replace(tempWritingStyleString, ""), false);
            }
            break;
    }

    if (defaultLanguageDropdown.value === "default" && writingToneDropdown.value === "default" && writingStyleDropdown.value === "default") {
        newValue = textarea.value.replace(backupPrompt, "").replace(forceFollowInstructions, "").replace("Instructions: \n", "").replace("Instructions: ", "");

        setChatbox(newValue);

        prompt = "";
        backupPrompt = "";
    } else {
        if (!prompt.includes("These instructions are") && prompt !== "")
            prompt += forceFollowInstructions;
    }

    if (e.target === defaultLanguageDropdown || e.target === writingToneDropdown || e.target === writingStyleDropdown) {
        browser.storage.local.set({
            "default_language_select": defaultLanguageDropdown.value,
            "writing_tone_select": writingToneDropdown.value,
            "writing_style_select": writingStyleDropdown.value
        }, () => {
            // Do nothing
        });

        /* The instructions stay per conversation, once inside a chat we don't need to manipulate the chatbox content anymore */
        if (!textarea.value.includes(prompt) && prompt !== "" && !isInsideChat()) {
            if (backupPrompt !== prompt) {
                setChatboxRaw(textarea.value.replace(backupPrompt, ""));
                backupPrompt = prompt;
            }

            setChatboxRaw(prompt + textarea.value, false);
        }
    }
});

document.body.addEventListener("click", (e) => {
    setTimeout(() => {
        const continueConversationButton = document.getElementById("continue-conversation-button");
        continueConversationButton.style.display = isInsideChat() ? "block" : "none";
    }, 1500);

    if (e.target.id === "save-default-model-button") {
        // Handle save button click
        const subTypeDropdown = document.getElementById("subscription_type");
        const dropdown = document.getElementById("gpt_version");

        browser.storage.local.set({
            "subscription_type": subTypeDropdown.value,
            "gpt_version": dropdown.value
        }, () => {
            // Do nothing
        });
    }

    if (e.target.id === "continue-conversation-button") {
        if (!window.location.href.includes("/c/")) {
            e.target.textContent = "Please start a chat";
            e.target.disabled = true;

            // Re-enable the button after a few seconds (e.g., 3 seconds)
            setTimeout(() => {
                e.target.innerHTML = "&#9654; Continue conversation";
                e.target.disabled = false;
            }, 3000); // Change 3000 to desired number of milliseconds
        } else {
            e.target.textContent = "Working...";
            e.target.disabled = true;

            const textarea = document.getElementById("prompt-textarea");
            const sendButton = document.querySelector("[data-testid='send-button']");

            if (!textarea) {
                console.error("Textarea could not be found.");
                return;
            }

            // TODO: Once language selection becomes a feature, try translating this into a few other languages as to not confuse ChatGPT
            textarea.value = "Go on, please. Continue what you were talking about and further expand on it.";

            // Dispatching this event is required to mimic manual user input. Without this, the plugin wouldn't be able to send the message on its own.
            const inputEvent = new Event("input", {
                "bubbles": true,
                "cancelable": true
            });
            textarea.dispatchEvent(inputEvent);

            if (sendButton) {
                sendButton.click();
            }

            // Re-enable the button after a few seconds (e.g., 3 seconds)
            setTimeout(() => {
                e.target.innerHTML = "&#9654; Continue conversation";
                e.target.disabled = false;
            }, 3000); // Change 3000 to desired number of milliseconds
        }
    }
});

// Helper function
function updateGPTVersionOptions(subscriptionType) {
    const gptVersionDropdown = document.getElementById("gpt_version");

    // Clear existing options
    gptVersionDropdown.textContent = "";

    if (subscriptionType === 'free') {
        const option = document.createElement("option");
        option.value = "text-davinci-002-render-sha";
        option.textContent = "GPT-3.5";
        gptVersionDropdown.appendChild(option);
    } else {
        const options = [
            { value: "gpt-4", text: "GPT-4" },
            { value: "gpt-4-browsing", text: "Browse with Bing" },
            { value: "gpt-4-code-interpreter", text: "Advanced Data Analysis" },
            { value: "gpt-4-plugins", text: "Plugins" },
            { value: "gpt-4-dalle", text: "DALL·E 3" }
        ];

        options.forEach(opt => {
            const option = document.createElement("option");
            option.value = opt.value;
            option.textContent = opt.text;
            gptVersionDropdown.appendChild(option);
        });
    }
}

function setChatboxRaw(text, clear) {
    const textarea = document.getElementById("prompt-textarea");

    textarea.value = text;
    if (clear)
        prompt = "";

    // Dispatching this event is required to mimic manual user input. Without this, the plugin wouldn't be able to send the message on its own.
    const inputEvent = new Event("input", {
        "bubbles": true,
        "cancelable": true
    });
    textarea.dispatchEvent(inputEvent);
    textarea.focus();
}

function setChatbox(text) {
    setChatboxRaw(text, true);
}

function isInsideChat() {
    if (window.location.href.includes("/c/"))
        return true;

    // this is the small "Click to send" arrow svg icon which is embedded into the preset message suggestions when outside of chats
    return document.querySelector('svg[class="icon-sm"][xmlns="http://www.w3.org/2000/svg"][viewBox="0 0 16 16"] > path[d="M.5 1.163A1 1 0 0 1 1.97.28l12.868 6.837a1 1 0 0 1 0 1.766L1.969 15.72A1 1 0 0 1 .5 14.836V10.33a1 1 0 0 1 .816-.983L8.5 8 1.316 6.653A1 1 0 0 1 .5 5.67V1.163Z"][fill="currentColor"]') === null;
}
